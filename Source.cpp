/*
Составить программу, которая будет генерировать случайные числа в интервале [a;b]
и заполнять ими двумерный массив размером 10 на 10.
В массиве необходимо найти номер строки с минимальным элементом.
Поменять строки массива местами, строку с минимальным элементом и первую строку массива.
Организовать удобный вывод на экран.
*/

#include <iostream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
using namespace std;

int main()
{
    setlocale(LC_ALL, "RU");

    srand(time(NULL));
    double a, b;
    cout << "Введите число а: ";
    cin >> a;
    cout << "Введите число в: ";
    cin >> b;

    double matrix[10][10];

    for (int counter_rows = 0; counter_rows < 10; counter_rows++)
        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
            matrix[counter_rows][counter_columns] = (double(rand()) / RAND_MAX) * (b - a) + a;

    cout << "\nСгенерированный массив вещественных чисел" << "\n";
    for (int counter_rows = 0; counter_rows < 10; counter_rows++)
    {
        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
            cout << setw(7) << fixed << setprecision(3) << matrix[counter_rows][counter_columns];
        cout << endl;
    }

    double min = matrix[0][0];
    int nom;

    for (int counter_rows = 0; counter_rows < 10; counter_rows++)
    {
        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
        {
            if (matrix[counter_rows][counter_columns] < min)
            {
                min = matrix[counter_rows][counter_columns];
                nom = counter_rows;
            }
        }
    }

    cout << "\nНомер строки с минимальным элементом: " << (nom + 1) << "\n";
    cout << "Минимальный элемент: " << min << "\n";
    if (nom != 0)
    {
        double temp[10];
        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
            temp[counter_columns] = matrix[0][counter_columns];

        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
            matrix[0][counter_columns] = matrix[nom][counter_columns];

        for (int counter_columns = 0; counter_columns < 10; counter_columns++)
            matrix[nom][counter_columns] = temp[counter_columns];

        cout << "\nМассив с перестановленными строками" << "\n";
        for (int counter_rows = 0; counter_rows < 10; counter_rows++)
        {
            for (int counter_columns = 0; counter_columns < 10; counter_columns++)
                cout << setw(7) << fixed << setprecision(3) << matrix[counter_rows][counter_columns];
            cout << "\n";
        }

    }

    return 0;
}